//
//  File.swift
//  InterviewTask
//
//  Created by Lalit Pratap on 18/03/20.
//  Copyright © 2020 Lalit Pratap. All rights reserved.
//

import Foundation

struct dataModel:Codable {
    let name: String?
    let sub_category:[subCategory]?
}

struct subCategory:Codable {
    let name: String?
    let display_name:String?
}
